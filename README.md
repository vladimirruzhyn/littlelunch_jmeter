The test script is located in the module_test.jmx file. In the csv folder are configuration files that store the necessary initial parameters for the test, such as - product ID, category name, quantity of goods in the purchase, etc.
For the test to work, it is necessary that the csv folder and the script are in the same folder.
After running the script, a report folder will be created in which the reports are located. Reports will be created for each controller in the test scenario separately.

For start. 
	Run the jmeter program. 
	In it open the script module_test.jmx and run.

Reports will be generated in the report folder which will be created after the script is executed.